package info.scce.cinco.product.chess.action

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import info.scce.cinco.product.chess.chess.Board
import info.scce.cinco.product.chess.util.ChessExtension

class BoardInitializeAction extends CincoCustomAction<Board> {
	
	extension ChessExtension = ChessExtension.instance
	
	override getName() {
		"Initialize board"
	}
	
	override hasDoneChanges() {
		true
	}
	
    override canExecute(Board it) {
    	true
    }
    
    override execute(Board it) {
    	initialize
    }
	
}
