id info.scce.cinco.product.chess
stylePath "model/Chess.style"

@event("info.scce.cinco.product.chess.event.TableEvent")
graphModel Table {
	diagramExtension "chess"
	containableElements (Board, Piece)
}



// Board

@event("info.scce.cinco.product.chess.event.BoardEvent")
@contextMenuAction("info.scce.cinco.product.chess.action.BoardInitializeAction")
@contextMenuAction("info.scce.cinco.product.chess.action.BoardPrepareAction")
@disable(resize)
@palette("Boards")
@icon("icons/boards/16x16/board.png")
container Board {
	style Board
	containableElements (Square, Label)
	attr EInt as horizontalSquareCount := "8"
	attr EInt as verticalSquareCount   := "8"
}

abstract node Label {
	attr EInt as position
	attr EString as name
}

@disable(create, delete, select, move, resize)
node HorizontalLabel extends Label {
	style HorizontalLabel("${name}")
}

@disable(create, delete, select, move, resize)
node VerticalLabel extends Label {
	style VerticalLabel("${name}")
}



// Square

abstract container Square {
	containableElements (Piece)
	incomingEdges (Help)
	attr EInt as horizontalPosition
	attr EInt as verticalPosition
	attr EString as name
}

@disable(create, delete, select, move, resize)
container BlackSquare extends Square {
	style BlackSquare
}

@disable(create, delete, select, move, resize)
container WhiteSquare extends Square {
	style WhiteSquare
}



// Piece

@event("info.scce.cinco.product.chess.event.PieceEvent")
abstract node Piece {
	incomingEdges (Help)
	outgoingEdges (Help)
}

@event("info.scce.cinco.product.chess.event.PawnEvent")
abstract node Pawn extends Piece {
	
}
	
@event("info.scce.cinco.product.chess.event.RookEvent")
abstract node Rook extends Piece {
	
}
	
@event("info.scce.cinco.product.chess.event.KnightEvent")
abstract node Knight extends Piece {
	
}
	
@event("info.scce.cinco.product.chess.event.BishopEvent")
abstract node Bishop extends Piece {
	
}
	
@event("info.scce.cinco.product.chess.event.QueenEvent")
abstract node Queen extends Piece {
	
}
	
@event("info.scce.cinco.product.chess.event.KingEvent")
abstract node King extends Piece {
	
}



// Black Piece

@palette("Black Pieces")
@icon("icons/pieces/16x16/black-pawn.png")
node BlackPawn extends Pawn {
	style BlackPawn
}
	
@palette("Black Pieces")
@icon("icons/pieces/16x16/black-rook.png")
node BlackRook extends Rook {
	style BlackRook
}
	
@palette("Black Pieces")
@icon("icons/pieces/16x16/black-knight.png")
node BlackKnight extends Knight {
	style BlackKnight
}
	
@palette("Black Pieces")
@icon("icons/pieces/16x16/black-bishop.png")
node BlackBishop extends Bishop {
	style BlackBishop
}
	
@palette("Black Pieces")
@icon("icons/pieces/16x16/black-queen.png")
node BlackQueen extends Queen {
	style BlackQueen
}
	
@palette("Black Pieces")
@icon("icons/pieces/16x16/black-king.png")
node BlackKing extends King {
	style BlackKing
}



// White Piece

@palette("White Pieces")
@icon("icons/pieces/16x16/white-pawn.png")
node WhitePawn extends Pawn {
	style WhitePawn
}
	
@palette("White Pieces")
@icon("icons/pieces/16x16/white-rook.png")
node WhiteRook extends Rook {
	style WhiteRook
}
	
@palette("White Pieces")
@icon("icons/pieces/16x16/white-knight.png")
node WhiteKnight extends Knight {
	style WhiteKnight
}
	
@palette("White Pieces")
@icon("icons/pieces/16x16/white-bishop.png")
node WhiteBishop extends Bishop {
	style WhiteBishop
}
	
@palette("White Pieces")
@icon("icons/pieces/16x16/white-queen.png")
node WhiteQueen extends Queen {
	style WhiteQueen
}
	
@palette("White Pieces")
@icon("icons/pieces/16x16/white-king.png")
node WhiteKing extends King {
	style WhiteKing
}



// Edge

@event("info.scce.cinco.product.chess.event.HelpEvent")
edge Help {
	style Help
}
